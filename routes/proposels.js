const express = require('express')
const controller = require('../controllers/proposels')
const router = express.Router()


router.get('/', controller.getProposels)

router.get('/:id', controller.updateProposal)
module.exports = router