const express = require('express')
const controller = require('../controllers/auth')
const passport = require('passport');
const router = express.Router()
router.post('/login', controller.login)
router.get('/check-login',passport.authenticate('jwt', {session: false}), controller.returnOk);
module.exports = router
