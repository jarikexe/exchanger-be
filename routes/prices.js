const express = require('express')
const controller = require('../controllers/proposels')
const router = express.Router()


router.post('/', controller.setPrice)
router.get('/', controller.getPrices)

module.exports = router