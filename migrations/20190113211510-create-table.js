'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
    'proposals',
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE
      },
      updatedAt: {
        type: Sequelize.DATE
      },
      proposals_id: Sequelize.STRING,
      recive: Sequelize.INTEGER,
      give: Sequelize.INTEGER,
    },
    {
      engine: 'InnoDB', // default: 'InnoDB'
      charset: 'utf8' // default: null
    }
  )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropAllTables();
  }
};
