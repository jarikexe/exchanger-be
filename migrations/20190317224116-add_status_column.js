'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        'proposals',
        'status',
        Sequelize.SMALLINT
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        'proposals',
        'status',
        Sequelize.SMALLINT
    );
  }
};
