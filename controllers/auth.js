const jwt = require('jsonwebtoken')
const configs = require('../config/env')

module.exports.login = function (req, res) {
  const login = req.body.login;
  const password = req.body.password;
  console.log(login);

  const user = {
    login:'admin',
    password:'12345'
  }

  if(login != user.login){
    res.status(404).json({
      message: "User is not found"
    });
  }else{
    if(password != user.password){
      res.status(401).json({
        message: "Password is incorrect"
      });
    }else{
      //success login, generate tocken.
      const tocken = jwt.sign({
        login: login
      }, configs.secretKey, {expiresIn: 60*60});

      res.status(200).json({
        tocken: `Bearer ${tocken}`
      })
    }
  }
}
module.exports.returnOk = function(req, res){
  res.status(200).json({
    status: 'Ok'
  });
}
