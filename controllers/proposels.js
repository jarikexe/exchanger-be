const db = require('../config/db');
const Proposels = require('../models/Proposels');
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./scratch');

    module.exports.getProposels = function (req, res) {
    Proposels.findAll({where: {status: 0}})
        .then(proposels => {
            res.status(200).json({
                proposels
            });
        })
}

module.exports.getPrices = function(req, res){
    res.status(200).json({
        usd: localStorage.getItem('usd'),
        btc: localStorage.getItem('btc')
    });
}
module.exports.setPrice = function(req, res){
    localStorage.setItem('usd', req.body.usd)
    localStorage.setItem('btc', req.body.btc)
    res.status(200).json({'status': 'ok'});
}

module.exports.addProposels = function(data, io){
    Proposels.create({
        give: data.give,
        createdAt: new Date(),
        updatedAt: new Date(),
        recive: data.recive,
        proposals_id: data.requestId,
        status: 0
    }).then(task => {
       task.save();
    });

    io.emit('proposalsAdm', data);
}

module.exports.updateProposal = function (req, res) {
    Proposels.find({ where: { proposals_id: req.params.id} })
        .then(proposal => {
        if (proposal) {
            proposal.update({
                status: 1
            })
                .then(() => {
                    res.status(200).json({
                       msg: 'Proposal was updated'
                    });
                })
        }
    })
}