const Sequelize = require('sequelize');
const db = require('../config/db');

const Proposels = db.define('proposals', {
    createdAt: {
        type: Sequelize.DATE
    },
    updatedAt: {
        type: Sequelize.DATE
    },
    proposals_id: {
        type: Sequelize.STRING
    },give: {
        type: Sequelize.INTEGER
    },
    recive: {
        type: Sequelize.INTEGER
    },
    status: {
        type: Sequelize.SMALLINT
    }
});

module.exports = Proposels;