const express             = require('express')
const bodyParser          = require('body-parser')
const passport            = require('passport')
const proposelsRoutes     = require('./routes/proposels')
const pricesRoutes     = require('./routes/prices')
const proposelController  = require('./controllers/proposels')
const authRouter          = require('./routes/auth')
const statusController    = require('./controllers/status')
var cors = require('cors')

var io = require('socket.io')(3001);

io.on('connection', (socket) => {

    console.log('new socket connetction');
      socket.on('proposals', function(data){
            console.log(data);
        proposelController.addProposels(data, io);
    });
    socket.on('statusMsg', function(data){
        statusController.addStatusMsg(data, io)
    });
    socket.on('statusFlag', function(data){
        statusController.addStatusFlag(data, io)
    });
});

const PORT = process.env.PORT || 5000

//database
const db = require('./config/db')

//test db
db.authenticate()
    .then(() => console.log('Database connected ✔'))
    .catch(err => console.log("Error: " + err))

    //sockets config
const app = express()
app.use(cors())
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  

app.use(express.static('./exchanger/build'));

app.use(passport.initialize())
require('./middleware/passport')(passport)

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())


//app.use('/socket.io/*', function() { return io } );
app.use('/api/v1/prices', pricesRoutes);
app.use('/api/v1/proposels', passport.authenticate('jwt', {session: false}), proposelsRoutes)
app.use('/api/v1/auth', authRouter)

app.get('^(?!/socket.io/*)*', (req, res) => {
    res.sendfile('exchanger/build/index.html')
}).listen(PORT, () => console.log(`Listening on ${ PORT }`));

server = require('http').createServer(app)

console.log('Socket listening on port ', PORT, '✔');

const socketRoutes = require('./routes/socket')

app.use('/socket', socketRoutes)


module.exports = app
