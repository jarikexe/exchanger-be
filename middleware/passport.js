const configs = require('../config/env')
const JwtStrategy = require('passport-jwt').Strategy,
      ExtractJwt = require('passport-jwt').ExtractJwt;

const opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = configs.secretKey;

module.exports = passport => {
    passport.use(
      new JwtStrategy(opts, (payload, done) => {
        const user = payload.login
        done(null, user)
    })
  )
}
