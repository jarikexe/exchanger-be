import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
//import Particles from 'react-particles-js'


class MainLayout extends Component {
      constructor(props) {
       super(props);
     }
	    render() {
        return (
           <div className='appWrapper'>
           <nav className="navbar navbar-expand-lg navbar-light bg-light">
              <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                  <li className="nav-item">
                    <Link className="nav-link" to="/">Home</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/exchange">Exchange</Link>
                  </li>
                </ul>
              </div>
            </nav>
             {this.props.children}
          </div>
        );
    }
}


export default MainLayout;
