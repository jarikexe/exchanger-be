import React, { Component } from 'react';
import './Home.css';


class Home extends Component {
    render() {
        return (
            <div className="homePage">
                <div className="homepageLogoWrapper">
                    <img className="homepageLogo" src="https://crypt-mining.net/assets/uploadify/v-news/advcash.png" alt="Logo"/>
                </div>
            </div>
        );
    }
}

export default Home;
