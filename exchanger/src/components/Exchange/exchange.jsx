import React, { Component } from 'react';
import openSocket from 'socket.io-client';
import uniqid from 'uniqid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import './Exchange.css';
import './statuses'
import Statuses from './statuses';
import axios from "axios";
const socket = openSocket.connect(window.location.hostname + ":3001");

const styles = theme => ({
    button: {
      display: 'block',
      marginTop: theme.spacing.unit * 2,
    },
    formControl: {
      margin: theme.spacing.unit,
      minWidth: 120,
    },
  });

class Exchnger  extends Component {
    constructor(props){
        super(props);
        this.state = {
            adminMsg: '',
            give: '',
            recive: '',
            giveCurrency:'BTC',
            reciveCurrency: 'USD',
            error: '',
            requestId: '',
            stamusMsg: '',
            statusMsgClass: '',
            statusFlag: '',
            usd: 'loading...',
            btc: 'loading...',
        }
        this.socket = openSocket(window.location.hostname + ":3001");
        this.socket.on('statusMsg', function (data) {
            this.setState({stamusMsg : data.msg, statusMsgClass: 'show '});
        }.bind(this));

        axios.get(`${window.Config.api}/prices`, {headers: {
                'Content-Type': 'application/json'
            }})
            .then( (response) => {
                const _prices = response.data;
                this.setState({usd: _prices.usd, btc: _prices.btc});
            });
    }
    sendData(data) {
        socket.emit('proposals', data);
      }
    handleSubmit = (e) => {
        e.preventDefault();
        this.state.requestId = uniqid();
        // window.history.pushState('exchange', 'Title', `client/exchange/${this.state.requestId }`);
        let data = this.state;
        this.sendData(data);
    }
    onGive(val){
        this.setState({
             give: val
        });
    }
    handleReciveChange (value){
        if(!Number.isInteger(parseInt(value))){
            this.state.error = 'All the filds should be numbers';
        }else{
            this.state.error = '';
        }
        this.setState({
             recive: value
        });
    }
    handleGiveCurrency (value){
        this.setState({
             giveCurrency: value
        });
        this.setState({ [value.target.name]: value.target.value });
    }
    handleReciveCurrency (value){
        this.setState({
             reciveCurrency: value
        });
    }
    render() {
        const { classes } = this.props;
        
        return (
            <div className="formWraper">
                <div className="container orderFormBox">
                    <form method="POST" onSubmit={this.handleSubmit}>
                        <div className="row">
                            <div className="col-6">
                                <TextField
                                    id="filled-full-width"
                                    label="Отдаете"
                                    style={{ margin: 8 }}
                                    fullWidth
                                    margin="normal"
                                    variant="filled"
                                   name="give"
                                   placeholder="Введие число"
                                   onChange={e => this.onGive(e.target.value)}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />

                            </div>
                            <div className="col-6 _dropdown">
                                <select
                                name="giveCurrency"
                                id="giveCurrency"
                                onChange={e => this.handleGiveCurrency(e.target.value)}
                                >
                                    <option value="btc">BTC</option>
			            
                                </select>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-6">

                                <TextField
                                   id="filled-full-width"
                                   label="Получаете"
                                   style={{ margin: 8 }}
                                   fullWidth
                                   name="recive"
                                   margin="normal"
                                   variant="filled"
                                   name="give"
                                   placeholder="Введие число"
                                   onChange={e => this.handleReciveChange(e.target.value)}
                                   InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </div>
                            <div className="col-6 _dropdown">
                                <select
                                name="reciveCurrency"
                                id="reciveCurrency"
                                onChange={e => this.handleReciveCurrency(e.target.value)}
                                >
                                  
					<option value="eth">ETH</option>
                                </select>
                            </div>
                        </div>
                        <div className={this.state.statusMsgClass + "custom_msg"}>
                            <span className="orderStatusHeader">Статус заявки</span>
                            <div>{this.state.stamusMsg}</div>
                        </div>
                        <Statuses />
			<div className="col-12">
                        <div className='text-center makeOrderBtn'>
                            <Button
                            type="submit"
                            className="btn btn-primary col-4 mr-auto"
                            variant="contained"
                            color="primary"
                            >시작하기</Button>
                        </div>

			<div className='text-center makeOrderBtn'>
                            <Button
                           
                            className="btn btn-primary col-4 mr-auto"
                            variant="contained"
                            color="danger"
                            >출금하기</Button>
                        </div>
                        </div>
                    </form>

                    {this.state.error}
                </div>
                <div className="text-center">
                    <h1>BTC: {this.state.btc}</h1>
                    <h1>USD: {this.state.usd}</h1>
                </div>

            </div>
        );
    }
}
 
export default Exchnger;
