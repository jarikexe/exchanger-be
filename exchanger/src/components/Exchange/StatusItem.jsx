import React, { Component } from 'react';
import openSocket from 'socket.io-client';

class StatusesItem  extends Component {
    state = {
        statuses: [{name : 'Start', status: ''}, {name : 'Processing', status: ''}, {name : 'Finish', status: ''}]
    }
    constructor(props) {
            super(props)
            this.socket = openSocket(window.location.hostname + ":3001");
            this.socket.on('statusFlag', function (data) {
                this.setState({statusFlag : data.status});
                this.updateStatus(data);
            }.bind(this));
        }
    updateStatus(data){
        let statusesCopy = this.state.statuses;

        statusesCopy.forEach(function(item, i, arr) {
            item.status = '';
          });
        if(data.status == 1){
            statusesCopy[0].status = 'active ';
        }else if(data.status == 2){
            statusesCopy[1].status = 'active ';
        }else if(data.status == 3){
            statusesCopy[2].status = 'active ';
        }
        this.setState({statuses: statusesCopy});
    }
    render() {
        return (
            <div className='stauses'>
                {this.state.statuses.map(status => <div className={status.status + "status_items"} key={status.name}>{status.name}</div>)}
            </div>
        );
    }
}

export default StatusesItem;
