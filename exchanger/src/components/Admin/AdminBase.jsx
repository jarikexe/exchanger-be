import React, { Component } from "react";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import "./Admin.css";

import axios from 'axios'
import Cookies from 'universal-cookie';

class Admin extends Component {

  constructor(props){
      super(props)
      const cookies = new Cookies();
      let tocken = cookies.get('Barier');
      axios.get(`${window.Config.api}/auth/check-login`, {headers: {
        'Authorization': tocken,
        'Content-Type': 'application/json'
      }})
        .then(response => {
            console.log('ok')
          }).catch((error) => {
          window.location.replace(`/login`);
        });
    }


  render() {
    const cookies = new Cookies();
    let tocken = cookies.get('Barier');
      return (
        <div>
        <div>
          <AppBar position="static">
            <Toolbar variant="dense">
              <Typography variant="h6" color="inherit">
                Admin
              </Typography>
            </Toolbar>
          </AppBar>
        </div>
          <AppBar />
          {this.props.children}
        </div>
      );
    }
}

export default Admin;
