import React, {Component} from "react";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import Draggable from 'react-draggable';
import Snackbar from '@material-ui/core/Snackbar';

import axios from 'axios';
import openSocket from 'socket.io-client';
import Cookies from 'universal-cookie';

class AdminHome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: "",
            statusFlag: "",
            proposals: [],
            usd: '',
            btc: ''
        }
        const cookies = new Cookies();
        let tocken = cookies.get('Barier');
        let self = this;

        axios.get(`${window.Config.api}/proposels`, {headers: {
            'Authorization': tocken,
            'Content-Type': 'application/json'
        }})
        .then(function (response) {
            const _proposals = response.data.proposels;
            _proposals.forEach(function (item) {
                let toAdd = {
                    requestId: item.proposals_id,
                    give:item.give,
                    recive: item.recive
                }
                let state_proposals = [...self.state.proposals];
                state_proposals.push(toAdd);
                self.setState({proposals: state_proposals});
            });
        });
        axios.get(`${window.Config.api}/prices`, {headers: {
                'Authorization': tocken,
                'Content-Type': 'application/json'
            }})
            .then(function (response) {
               const _prices = response.data;
               console.log(_prices);
               self.setState({usd: _prices.usd, btc: _prices.btc});
            });
        this.socket = openSocket(window.location.hostname + ":3001");
        this.socket.on('proposalsAdm', function (data) {
            this.setState({proposals: this.state.proposals.concat(data)});
            const audio = new Audio('./open-ended.mp3');
            audio.play();
        }.bind(this));
    }

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.state.status) {
            this.socket.emit('statusMsg',
                {
                    "msg": this.state.status
                }
            );
        } else {
            this.state.error = "Сообщение не может быть пустым"
        }
    }

    handleStatusMsg(value) {
        this.setState({
            status: value
        });
    }

    updateStatusFlag(status) {
        this.socket.emit('statusFlag',
            {
                "status": status
            }
        );
    }

    setStart() {
        this.setState({
            statusFlag: 1
        });
        this.updateStatusFlag(1);
    }

    setProgress() {
        this.setState({
            statusFlag: 2
        });
        this.updateStatusFlag(2);
    }

    setFinish() {
        this.setState({
            statusFlag: 3
        });
        this.updateStatusFlag(3);
    }

    okClick(id) {
        const cookies = new Cookies();
        let tocken = cookies.get('Barier');

        const _proposals = [...this.state.proposals];
        let object = _proposals.findIndex(obj => {
            return obj.requestId === id
        });
        axios.get(`${window.Config.api}/proposels/${id}`,

            {headers: {
                    'Authorization': tocken,
                    'Content-Type': 'application/json'
                }});
        _proposals.splice(object, 1);
        this.setState({proposals: _proposals});
    }
    savePrice(){
        const cookies = new Cookies();
        let tocken = cookies.get('Barier');

        axios.post(`${window.Config.api}/prices`, {headers: {
                'Authorization': tocken,
                'Content-Type': 'application/json'
            }, usd: this.state.usd, btc: this.state.btc});
    }

    render() {
        return (
            <div className="Admin">
                <div className="cards-1">
                    <Card className="item">
                        <CardContent>
                            <Typography color="textSecondary" gutterBottom>
                                <h2>Messeg</h2>
                            </Typography>
                            <form method="POST" onSubmit={this.handleSubmit}>
                                <TextField
                                    id="filled-full-width"
                                    label="Введите сообщение которое увидет пользователь"
                                    placeholder="Статус заявки"
                                    fullWidth
                                    margin="normal"
                                    variant="filled"
                                    onChange={e => this.handleStatusMsg(e.target.value)}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                                <Button
                                    type="submit"
                                    variant="contained"
                                    size="large"
                                >
                                    <SaveIcon/>
                                    Update
                                </Button>
                                <div className='error'></div>
                            </form>
                        </CardContent>
                    </Card>
                    <Card className="item">
                        <CardContent>
                            <Typography color="textSecondary" gutterBottom>
                                <h2>Status</h2>
                            </Typography>
                            <div className="statusesBtn">
                                <Button
                                    variant="contained"
                                    size="large"
                                    margin
                                    className='item'
                                    onClick={e => this.setStart(e.target.value)}
                                    color="primary">
                                    Start
                                </Button>
                                <Button
                                    variant="contained"
                                    size="large"
                                    margin
                                    className='item'
                                    onClick={e => this.setProgress(e.target.value)}
                                    color="primary">
                                    Processing
                                </Button>
                                <Button
                                    variant="contained"
                                    size="large"
                                    margin
                                    className='item'
                                    onClick={e => this.setFinish(e.target.value)}
                                    color="primary">
                                    Finish
                                </Button>
                            </div>
                        </CardContent>
                    </Card>
                    <Card className="item">
                        <CardContent>
                            <Typography color="textSecondary" gutterBottom>
                                <h2>Prices</h2>
                            </Typography>
                            <TextField
                                id="filled-full-width"
                                label="BTC"
                                value={this.state.btc}
                                placeholder="00.000"
                                fullWidth
                                margin="normal"
                                variant="filled"
                                onChange={e => this.setState({btc: e.target.value})}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                            <TextField
                                id="filled-full-width"
                                label="USD"
                                placeholder="00.00"
                                fullWidth
                                value={this.state.usd}
                                margin="normal"
                                variant="filled"
                                onChange={e => this.setState({usd: e.target.value})}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                            <Button
                                onClick={e=> this.savePrice()}
                                variant="contained"
                                color="secondary">
                                Save
                            </Button>
                        </CardContent>
                    </Card>
                    <div className="absoluteDiv">
                        {this.state.proposals.map(msg =>
                            <Draggable key={msg.requestId} >
                                <Card className='drugableCard'>
                                    <CardContent>
                                        <p>Code: {msg.requestId}</p>
                                        <p>Recive amount: {msg.recive} </p>
                                        <p>Give amount: {msg.give}</p>
                                        <Button
                                            onClick={() => this.okClick(msg.requestId)}
                                            variant="contained"
                                            color="primary">
                                            시작하기
                                        </Button>
                                        <Button
                                                onClick={() => this.okClick(msg.requestId)}
                                                variant="contained"
                                                color="secondary">
                                            출금하기
                                        </Button>
                                    </CardContent>
                                </Card>
                            </Draggable>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

export default AdminHome;
