import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

class Navigation extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li className="nav-item">
                            <Link className="nav-link" to="/client/home">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/client/exchange">Exchange</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Navigation;



