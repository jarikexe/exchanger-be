import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import Home from './components/Home/home'
import ExchangePage from './components/Exchange/exchange'
import AdminBase from './components/Admin/AdminBase'
import AdminHome from './components/Admin/AdminHome'
import MainLayout from './MainLayout'
import SingIn from './components/Authorization/SingIn'

import './App.css';

import 'bootstrap/dist/css/bootstrap.css';

const RouteWithLayout = ({layout, component, ...rest}) => (
    <Route {...rest} render={(props) =>
      React.createElement( layout, props, React.createElement(component, props))
    }/>
);

class App extends Component {
  render() {
    return (
      <Router>
        <React.Fragment>
          <RouteWithLayout layout={MainLayout} exact path="/" component={Home}/>
          <RouteWithLayout layout={MainLayout} path="/exchange" component={ExchangePage} />
          <RouteWithLayout layout={MainLayout} path="/login" component={SingIn} />
          <RouteWithLayout layout={AdminBase} path="/admin" component={AdminHome}/>
        </React.Fragment>
      </Router>
    );
  }
}

export default App;
